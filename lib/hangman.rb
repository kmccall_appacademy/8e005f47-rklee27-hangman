class Hangman
  attr_reader :guesser, :referee, :board

  def initialize(players)
    @guesser = players[:guesser]
    @referee = players[:referee]
  end

  def setup
    secret_length = @referee.pick_secret_word
    @guesser.register_secret_length(secret_length)
    @board = "_" * secret_length
  end

  def take_turn
    guess = @guesser.guess
    indices = @referee.check_guess(guess)
    update_board(guess, indices)
    @guesser.handle_response
  end

  def update_board(guess, indices)
    indices.each { |idx| @board[idx] = guess }
  end

  def handle_response

  end
end

class HumanPlayer
  def handle_response
  end
end

class ComputerPlayer

  attr_reader :candidate_words

  def initialize(dictionary)
    @dictionary = dictionary
  end

  def pick_secret_word
    @dictionary[0].length
  end

  def check_guess(guess)
    right_letters = []
    @dictionary[0].each_char.with_index do |char, idx|
      if char == guess
        right_letters << idx
      end
    end
    right_letters
  end

  def register_secret_length(length)
    @candidate_words = @dictionary.select { |word| word.length == length }
  end

  def guess(board)
    freq_table = frequency(board)

    freq_letters = freq_table.sort_by { |letter, count| count }
    letter, _ = freq_letters.last

    letter
  end

  def handle_response(guess, response_indices)
    @candidate_words.reject! do |word|
      should_delete = false
        word.split("").each_with_index do |letter, index|
          if (letter == guess) && (!response_indices.include?(index))
            should_delete = true
            break
          elsif (letter != guess) && (response_indices.include?(index))
            should_delete = true
            break
          end
        end
        should_delete
    end
  end

  private

  def frequency(board)
    frequency = Hash.new(0)
    @candidate_words.each do |word|
      board.each_with_index do |letter, idx|
        frequency[word[idx]] += 1 if letter.nil?
      end
    end
    frequency
  end
end
